Credits
=======

Author: Tao "DePierre" Sauvage
License: WTFPL v2


Summary
=======

New version of my personnal blog (previous repo blog-depierre).
It's still written in python with Flask framework but a lot of improvements
have been done.


Requierements
=============

- Flask-SQLAlchemy for the database
- Flask-Babel for the translation
- Flask-SuperAdmin for the administration
- pygments for the syntaxic coloration

Almost the entire code have been rewritten, therefore here is a new repo :/
