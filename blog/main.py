# -*- coding: utf-8 -*-
"""
    Main file of the blog
"""


import hashlib
from sets import Set
import pygments.formatters
from flask import request, g, render_template, session, redirect, url_for
from flask.ext.babel import refresh
from blog.database import Post, Category, Tag
from blog import app
from blog import db
from blog import babel


# Babel
@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])


# Blog
# Root of the blog a.k.a index
@app.route('/')
def index():
    """Query the last past"""

    post = Post.query.filter(Post.public).order_by(
        Post.pub_date.desc()
    ).first()

    return render_template('index.html', post=post)


# Process all the stuff concerning posts
@app.route('/posts/')
def show_posts():
    """Query all the posts"""

    posts = Post.query.order_by(
        Post.pub_date.desc()
    ).filter(Post.public).all()

    return render_template('posts.html', posts=posts)


@app.route('/posts/<int:id>')
def show_post(id):
    """Query one specific post"""

    if 'logged_in' in session and session['logged_in']:
        post = Post.query.filter(Post.id == id).first_or_404()
    else:
        post = Post.query.filter(Post.id == id, Post.public).first_or_404()

    return render_template('post.html', post=post)


# Process all the stuff concerning categories
@app.route('/categories/')
def show_categories():
    """Query all the categories"""

    categories = Category.query.order_by(Category.name).all()

    return render_template('categories.html', categories=categories)


@app.route('/categories/<int:id>')
def show_category(id):
    """Query one specific category"""

    category = Category.query.filter(Category.id == id).first_or_404()

    return render_template('category.html', category=category)


# Process login stuff
@app.route('/login/', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        hash_password = hashlib.sha512()
        hash_password.update(request.form['password'])
        if request.form['username'] == app.config['USERNAME'] \
                and hash_password.hexdigest() == app.config['PASSWORD']:
            session['logged_in'] = True
            return redirect(url_for('admin.index'))

    return render_template('login.html')


# Process search request
@app.route('/search/', methods=['GET', 'POST'])
@app.route('/search/<tag>', methods=['GET', 'POST'])
def search(tag=None):
    """Query posts which have 'tag' in its tags"""

    tags = tag
    if request.method == 'POST':
        tags = request.form['tags'].split()

    result = Set()
    posts = Post.query.all()
    for post in posts:
        for tag in post.tags:
            if tag.label in tags:
                result.update([post])
                break
    return render_template('posts.html', posts=result)


@app.route('/logout/')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('index'))


@app.route('/license/')
def license():
    return render_template('license.html')


@app.route('/about/')
def about():
    return render_template('about.html')


# Error handlers
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# Generate css from pygments
def pygments_style_defs(style='default'):
    """:return: the CSS definitions for the `Codehilite`_ Markdown plugin.

    :param style: The Pygments `style`_ to use.

    Only available if `Pygments`_ is.

    .. _Codehilite: http://www.freewisdom.org/projects/python-markdown/CodeHilite
    .. _Pygments: http://pygments.org/
    .. _style: http://pygments.org/docs/styles/
    """

    formater = pygments.formatters.HtmlFormatter(style=style)
    return formater.get_style_defs('.codehilite')


@app.route('/pygments.css')
def pygments_css():
    return pygments_style_defs('friendly'), 200, {'Content-Type': 'text/css'}
