# -*- coding: utf-8 -*-
"""
    Global definitions for the application
"""


from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.babel import Babel


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
db = SQLAlchemy(app)
babel = Babel(app)


import blog.database
import blog.main
import blog.admin
