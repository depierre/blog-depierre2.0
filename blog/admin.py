# -*- coding: utf-8 -*-
"""
    Main file of the blog
"""


import os
import os.path as op
from flask import session, render_template
from flask.ext.superadmin import Admin, AdminIndexView, expose
from flask.ext.superadmin.contrib import sqlamodel, fileadmin
from flask.ext.superadmin.model import filters, ModelAdmin
from flask.ext.superadmin.contrib.fileadmin import FileAdmin
from blog.database import Post, Category, Tag
from blog import app, db, babel


class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        if 'logged_in' in session and session['logged_in']:
            return True
        return False

    @expose('/')
    def index(self):
        return render_template('admin_index.html', admin_view=self)


class PostAdmin(ModelAdmin):
    def is_accessible(self):
        if 'logged_in' in session and session['logged_in']:
            return True
        return False

    session = db.session


class CategoryAdmin(ModelAdmin):
    def is_accessible(self):
        if 'logged_in' in session and session['logged_in']:
            return True
        return False

    session = db.session


class TagAdmin(ModelAdmin):
    def is_accessible(self):
        if 'logged_in' in session and session['logged_in']:
            return True
        return False

    session = db.session


class ImagesAdmin(FileAdmin):
    def is_accessible(self):
        if 'logged_in' in session and session['logged_in']:
            return True
        return False

    # Allowed extensions
    allowed_extensions = ('jpg', 'jpeg', 'png', 'gif')

    def __init__(self):
        path = op.join(op.dirname(__file__), 'static/images')
        try:
            os.mkdir(path)
        except OSError:
            pass
        super(ImagesAdmin, self).__init__(
            path,
            '/static/images/',
            name='Images'
        )


# Create admin
admin = Admin(app, name='Blog DePierre', index_view=MyAdminIndexView())
# Add views
admin.register(Post, PostAdmin)
admin.register(Category, CategoryAdmin)
admin.register(Tag, TagAdmin)
admin.add_view(ImagesAdmin())
