# -*- coding: utf-8 -*-
"""
    Definition of the tables for the database.
"""


from blog import db


# Many-to-many relation between Category and Post
categories = db.Table(
    'categories',
    db.Column('category_id', db.Integer, db.ForeignKey('category.id')),
    db.Column('post_id', db.Integer, db.ForeignKey('post.id'))
)
# Many-to-many relation between Tag and Post
tags = db.Table(
    'tags',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('post_id', db.Integer, db.ForeignKey('post.id'))
)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    body = db.Column(db.Text)
    pub_date = db.Column(db.Date)
    lang = db.Column(db.String(2))
    public = db.Column(db.Boolean)

    categories = db.relationship(
        'Category', secondary=categories,
        backref=db.backref('posts', lazy='dynamic')
    )

    tags = db.relationship(
        'Tag', secondary=tags,
        backref=db.backref('posts', lazy='dynamic')
    )

    def __repr__(self):
        return 'Post (%r)' % self.title


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))

    def __repr__(self):
        return 'Category (%r)' % self.name


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(120))

    def __repr__(self):
        return 'Tag (%r)' % self.label


# Creation of the database
db.create_all()
